package com.silva.commsreminder.settings;

import android.content.Context;

import com.silva.commsreminder.R;

/**
 * This class is dedicated to manage all the Call related settings
 */
public class CallSettings extends Settings {

    // public methods

    public static boolean isNotifyOnPendingCall(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_call_notify),
                context.getResources().getBoolean(R.bool.settings_call_notify_default));
    }

//    public static void setNotifyOnPendingCall(Context context, boolean notify){
//        init(context);
//        editor.putBoolean(context.getString(R.string.settings_call_notify), notify);
//        editor.commit();
//    }

    public static boolean isVibrateOnPendingCall(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_call_vibrate),
                context.getResources().getBoolean(R.bool.settings_call_vibrate_default));
    }

//    public static void setVibrateOnPendingCall(Context context, boolean vibrate){
//        init(context);
//        editor.putBoolean(context.getString(R.string.settings_call_vibrate), vibrate);
//        editor.commit();
//    }

    public static boolean isRingOnPendingCall(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_call_ring),
                context.getResources().getBoolean(R.bool.settings_call_ring_default));
    }

//    public static void setRingOnPendingCall(Context context, boolean ring){
//        init(context);
//        editor.putBoolean(context.getString(R.string.settings_call_ring), ring);
//        editor.commit();
//    }

    public static boolean isRingOnCallStream(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_call_stream),
                context.getResources().getBoolean(R.bool.settings_call_stream_default));
    }

//    public static void setRingOnCallStream(Context context, boolean stream){
//        init(context);
//        editor.putBoolean(context.getString(R.string.settings_call_stream), stream);
//        editor.commit();

    public static boolean isScreenOnPendingCall(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_call_screen),
                context.getResources().getBoolean(R.bool.settings_call_screen_default));
    }

//    public static void setScreenPendingCall(Context context, boolean screen){
//        init(context);
//        editor.putBoolean(context.getString(R.string.settings_call_screen), screen);
//        editor.commit();
//    }
//    }

    public static int getNotifyPeriodOnPendingCall(Context context){
        init(context);
        return Integer.parseInt(sharedPreferences.getString(
                context.getString(R.string.settings_call_period),
                context.getString(R.string.settings_call_period_default)));
    }

//    public static void setNotifyPeriodOnPendingCall(Context context, int period){
//        init(context);
//        editor.putString(context.getString(R.string.settings_call_period), String.valueOf(period));
//        editor.commit();
//    }

    public static int getTimeoutOnPendingCall(Context context){
        init(context);
        return Integer.parseInt(sharedPreferences.getString(
                context.getString(R.string.settings_call_timeout),
                context.getString(R.string.settings_call_timeout_default)));
    }

//    public static void setTimeoutPeriodOnPendingCall(Context context, int timeout){
//        init(context);
//        editor.putString(context.getString(R.string.settings_call_timeout), String.valueOf(timeout));
//        editor.commit();
//    }
}
