package com.silva.commsreminder.settings;

import android.content.Context;

import com.silva.commsreminder.R;

/**
 * This class is dedicated to manage all the Sms related settings
 */
public class SmsSettings extends Settings {

    // public methods

    public static boolean isNotifyOnPendingMessage(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_sms_notify),
                context.getResources().getBoolean(R.bool.settings_sms_notify_default));
    }

    public static boolean isVibrateOnPendingMessage(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_sms_vibrate),
                context.getResources().getBoolean(R.bool.settings_sms_vibrate_default));
    }

    public static boolean isRingOnPendingMessage(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_sms_ring),
                context.getResources().getBoolean(R.bool.settings_sms_ring_default));
    }

    public static boolean isRingOnCallStream(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_sms_stream),
                context.getResources().getBoolean(R.bool.settings_sms_stream_default));
    }

    public static boolean isScreenOnPendingMessage(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_sms_screen),
                context.getResources().getBoolean(R.bool.settings_sms_screen_default));
    }

    public static int getNotifyPeriodOnPendingMessage(Context context){
        init(context);
        return Integer.parseInt(sharedPreferences.getString(
                context.getString(R.string.settings_sms_period),
                context.getString(R.string.settings_sms_period_default)));
        // ListPreferences uses string values only
    }

    public static int getTimeoutOnPendingMessage(Context context){
        init(context);
        return Integer.parseInt(sharedPreferences.getString(
                context.getString(R.string.settings_sms_timeout),
                context.getString(R.string.settings_sms_timeout_default)));
    }

    public static boolean isVibrateOnNewMessage(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_sms_vibrate_new),
                context.getResources().getBoolean(R.bool.settings_sms_vibrate_new_default));
    }
}
