package com.silva.commsreminder.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.silva.commsreminder.R;

/**
 * This class is to manage all the settings to and from the private shared preferences.
 * Used as base class to obtain the SharedPreferences objects.
 */
public class Settings {

    // private fields

    private static final String TAG = "Settings";

    protected static SharedPreferences sharedPreferences = null;
    protected static SharedPreferences.Editor editor = null;

    // private methods

    protected static void init(Context context){
        initSharedPreferences(context);
        initEditor(sharedPreferences);
    }

    private static void initSharedPreferences(Context context){
        if(sharedPreferences == null){
            if(context == null)
                Log.e(TAG, "getSharedPreferences() failed due to null context");// sharedPreferences remains null
            else
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    private static void initEditor(SharedPreferences sharedPreferences){
        if(editor == null){
            if(sharedPreferences == null)
                Log.e(TAG, "getEditor() failed due to null sharedPreferences");// editor remains null
            else
                editor = sharedPreferences.edit();
        }
    }

    // public methods

    public static boolean isNotify(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_global_enabled),
                context.getResources().getBoolean(R.bool.settings_global_enabled_default));
    }

    public static void setNotify(Context context, boolean notify){
        init(context);
        editor.putBoolean(context.getString(R.string.settings_global_enabled), notify);
        editor.commit();
    }

    public static boolean isMute(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_global_mute),
                context.getResources().getBoolean(R.bool.settings_global_mute_default));
    }

    public static void setMute(Context context, boolean mute){
        init(context);
        editor.putBoolean(context.getString(R.string.settings_global_mute), mute);
        editor.commit();
    }

}
