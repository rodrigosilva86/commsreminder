package com.silva.commsreminder.settings;

import android.content.Context;

import com.silva.commsreminder.R;

import org.bostonandroid.timepreference.TimePreference;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * This class is dedicated to manage all the Sleep related settings
 *
 * Since all the settings are altered through the native Settings screen there is no need to create setters.
 */
public class SleepSettings extends Settings {

    // public methods

    public static boolean isEnabled(Context context){
        init(context);
        return sharedPreferences.getBoolean(
                context.getString(R.string.settings_sleep_enabled),
                context.getResources().getBoolean(R.bool.settings_sleep_enabled_default));
    }

    public static Calendar getStartTime(Context context){
        init(context);
        return TimePreference.getTimeFor(
                sharedPreferences,
                context.getString(R.string.settings_sleep_start),
                context.getString(R.string.settings_sleep_start_default));
    }

    public static Calendar getEndTime(Context context){
        init(context);
        return TimePreference.getTimeFor(
                sharedPreferences,
                context.getString(R.string.settings_sleep_end),
                context.getString(R.string.settings_sleep_end_default));
    }

    public static boolean isSleeping(Context context){
        if(isEnabled(context)){

            Calendar now = Calendar.getInstance();
            Calendar start = getStartTime(context);
            Calendar end = getEndTime(context);

            return isNowAfterStartAndBeforeEnd(now, start, end);

        }else{
            return false;
        }
    }


    // private methods

    private static boolean isNowAfterStartAndBeforeEnd(Calendar now, Calendar start, Calendar end){

        int nowHourMin = getHourMin(now);
        int startHourMin = getHourMin(start);
        int endHourMin = getHourMin(end);

        if(startHourMin <= endHourMin){
        //START is lower than END -> they are in the same day

            // if after start
            // and
            // if before end
            // then it's sleep time
            return (nowHourMin >= startHourMin) && ( nowHourMin <= endHourMin);

        }else{
        // START is higher than END -> they are in different days

            // if after start and before midnight
            // or
            // if before start and after midnight
            // then it's sleep time
            return (nowHourMin >= startHourMin) || (nowHourMin <= endHourMin);

        }

    }

    private static int getHourMin(Calendar calendar){
        // convert HH:MM into a number that can easily be compared
        // HH:MM -> HH * 100 + MM -> HHMM
        // 11:54 -> 11 * 100 + 54 -> 1154
        // day spans from 0000 to 2359 with gaps from [HH60 : HH99]
        int hourMin =   calendar.get(Calendar.HOUR_OF_DAY) * 100
                            +
                        calendar.get(Calendar.MINUTE);

        return hourMin;
    }

}
