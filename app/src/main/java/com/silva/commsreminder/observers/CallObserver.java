package com.silva.commsreminder.observers;


import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.CallLog;
import android.util.Log;

import com.silva.commsreminder.interfaces.CallStatusChangeListener;

public class CallObserver extends ContentObserver{

    // private fields
    private static final String TAG = "CallObserver";

    private ContentResolver cr;
    private CallStatusChangeListener callListener;

    // public fields
    public static final Uri CONTENT_URI = CallLog.Calls.CONTENT_URI;

    // public methods

    public CallObserver(Handler handler, ContentResolver cr, CallStatusChangeListener callListener){
        super(handler);
        this.cr = cr;
        this.callListener = callListener;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Log.d(TAG, "onChange() called");

        checkForMissedCalls();

    }

    public boolean hasMissedCalls(){
        return getMissedCallsCount() > 0;
    }

    /**
     * Check if there are any pending missed calls
     */
    public void checkForMissedCalls() {
        try{

            int missedCallsCount = getMissedCallsCount();

            if(missedCallsCount > 0){
                // there are unseen missed calls
                callListener.onCallPending(missedCallsCount);
            }else{
                // all missed calls have been seen
                callListener.onCallDismissed();
            }

        }catch(Exception e){
            // prevent any problem by silencing all the possible exception
            Log.d(TAG, "Could not access the call logs!", e);
        }
    }


    // private methods

    private int getMissedCallsCount(){
        //TODO: check if API14 IS_READ should be exploited
        // with API >=14 we can use IS_READ
        // with API <14 we have only NEW

        // Select columns of Type and New
        String columns[] = new String[] { CallLog.Calls.TYPE, CallLog.Calls.NEW };

        // Where Type = missed and New = true
        String where =  CallLog.Calls.TYPE + "=" + CallLog.Calls.MISSED_TYPE +
                " AND " + CallLog.Calls.NEW + "=1";

        Cursor cursor = cr.query(CONTENT_URI, columns, where, null, null);
        int missedCallsCount = 0;
        if(cursor != null){
            missedCallsCount = cursor.getCount();
            cursor.close();
        }

        Log.d(TAG, "missedCallsCount = " + missedCallsCount);
        return missedCallsCount;
    }
}
