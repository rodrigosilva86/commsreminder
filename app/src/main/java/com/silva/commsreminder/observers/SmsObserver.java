package com.silva.commsreminder.observers;


import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import com.silva.commsreminder.interfaces.SmsStatusChangeListener;

public class SmsObserver extends ContentObserver{

    // private fields
    private static final String TAG = "SmsObserver";

    private ContentResolver cr;
    private SmsStatusChangeListener smsListener;

    // public fields
    public static final Uri CONTENT_URI = Uri.parse("content://sms");

    // public methods

    public SmsObserver(Handler handler, ContentResolver cr, SmsStatusChangeListener smsListener){
        super(handler);
        this.cr = cr;
        this.smsListener = smsListener;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Log.d(TAG, "onChange() called");

        checkForPendingMessages();

    }

    public boolean hasPendingMessages(){
        return getPendingMessagesCount() > 0;
    }

    /**
     * Check if there are any pending messages
     */
    public void checkForPendingMessages() {
        try{

            int pendingMessagesCount = getPendingMessagesCount();

            if(pendingMessagesCount > 0){
                // there are unseen messages
                smsListener.onSmsPending(pendingMessagesCount);
            }else{
                // all pending messages have been read
                smsListener.onSmsRead();
            }

        }catch(Exception e){
            // prevent any problem by silencing all the possible exception
            Log.e(TAG, "Could not access the message table!", e);
        }
    }


    // private methods

    private int getPendingMessagesCount(){
        // Select read column
        String columns[] = new String[] { "read" };

        // Where read = false
        String where =  "read = 0";

        Cursor cursor = cr.query(CONTENT_URI, columns, where, null, null);
        int pendingMessagesCount = 0;
        if(cursor != null){
            pendingMessagesCount = cursor.getCount();
            cursor.close();
        }

        Log.d(TAG, "pendingMessagesCount = " + pendingMessagesCount);
        return pendingMessagesCount;
    }
}
