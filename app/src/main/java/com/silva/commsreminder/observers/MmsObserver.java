package com.silva.commsreminder.observers;


import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import com.silva.commsreminder.interfaces.MmsStatusChangeListener;

public class MmsObserver extends ContentObserver{

    // private fields
    private static final String TAG = "MmsObserver";

    private ContentResolver cr;
    private MmsStatusChangeListener mmsListener;

    // public fields
    public static final Uri CONTENT_URI = Uri.parse("content://mms");

    // public methods

    public MmsObserver(Handler handler, ContentResolver cr, MmsStatusChangeListener mmsListener){
        super(handler);
        this.cr = cr;
        this.mmsListener = mmsListener;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Log.d(TAG, "onChange() called");

        checkForPendingMessages();

    }

    public boolean hasPendingMessages(){
        return getPendingMessagesCount() > 0;
    }

    /**
     * Check if there are any pending messages
     */
    public void checkForPendingMessages() {
        try{

            int pendingMessagesCount = getPendingMessagesCount();

            if(pendingMessagesCount > 0){
                // there are unseen messages
                mmsListener.onMmsPending(pendingMessagesCount);
            }else{
                // all pending messages have been read
                mmsListener.onMmsRead();
            }

        }catch(Exception e){
            // prevent any problem by silencing all the possible exception
            Log.e(TAG, "Could not access the message table!", e);
        }
    }


    // private methods

    private int getPendingMessagesCount(){
        // Select read column
        String columns[] = new String[] { "read" };

        // Where read = false
        String where =  "read = 0";

        Cursor cursor = cr.query(CONTENT_URI, columns, where, null, null);
        int pendingMessagesCount = 0;
        if(cursor != null){
            pendingMessagesCount = cursor.getCount();
            cursor.close();
        }

        Log.d(TAG, "pendingMessagesCount = " + pendingMessagesCount);
        return pendingMessagesCount;
    }
}
