package com.silva.commsreminder.ui;

import com.silva.commsreminder.R;
import com.silva.commsreminder.service.ReminderService;

import android.app.AlertDialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class EntryActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_entry);

        // make sure the service is running
        Intent serviceIntent = new Intent(this, ReminderService.class);
        startService(serviceIntent);

        // fill in the version number
        TextView tvEntryVersion = (TextView)findViewById(R.id.tvEntryVersion);
        String version = tvEntryVersion.getText().toString();
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = String.format(version, pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            version = "";
        }
        tvEntryVersion.setText(version);

        // to ensure that the widget will become visible for Android 4.X
        sendBroadcast(new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
	}

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        openOptionsMenu();// since the screen is so empty, show the menu to the user
    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.entry, menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_about){
            showAbout();
        }
        if(item.getItemId() == R.id.action_settings){
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    // private methods

    private void showAbout(){
        // Creating alert Dialog with one Button
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        // Setting Dialog Title
        alertDialog.setTitle(R.string.app_name);

        // Setting Dialog Message
        View view = getLayoutInflater().inflate(R.layout.about, null);
        alertDialog.setView(view);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            TextView tvVersion = (TextView)view.findViewById(R.id.tvAboutVersion);
            tvVersion.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
        }

        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.telephone_black_128);

        // Showing Alert Message
        alertDialog.show();
    }
}
