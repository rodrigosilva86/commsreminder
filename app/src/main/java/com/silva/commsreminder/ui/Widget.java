package com.silva.commsreminder.ui;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.silva.commsreminder.R;
import com.silva.commsreminder.settings.Settings;

public class Widget extends AppWidgetProvider {

    // private fields

    private static final String WIDGET_CLICKED = "com.silva.commsreminder.Widget.WIDGET_CLICKED";

    // public methods


    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if(WIDGET_CLICKED.equals(action)){
            boolean enabled = Settings.isNotify(context);
            boolean mute = Settings.isMute(context);

            // there are 3 states and the sequence is:
            // 1 - enabled !mute
            // 2 - enabled mute
            // 3 - !enabled !mute

            if(enabled){ // enabled

                if(mute){// enabled and mute
                    enabled = false;
                    mute = false;
                }else{ // enabled and not mute
                    enabled = true;
                    mute = true;
                }

            }else{ // not enabled and don't care on mute

                enabled = true;
                mute = false;

            }

            Settings.setNotify(context, enabled);
            Settings.setMute(context, mute);
            updateWidgets(context);
        }else{
            super.onReceive(context, intent);
        }
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        updateWidgets(context, appWidgetManager, appWidgetIds);
    }


    // private methods

    private void updateWidgets(Context context){
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] widgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, Widget.class));
        updateWidgets(context, appWidgetManager, widgetIds);
    }

    private void updateWidgets(Context context, AppWidgetManager appWidgetManager,  int[] widgetIds){
        for (int widgetId : widgetIds) {
            updateWidget(context, appWidgetManager, widgetId);
        }
    }

    private void updateWidget(Context context, AppWidgetManager appWidgetManager, int widgetId){

        // Create intents to launch UI and change enabled state
        Intent intentUi = new Intent(context, EntryActivity.class);
        PendingIntent pendingIntentUi = PendingIntent.getActivity(context, 0, intentUi, 0);

        Intent intentEnabled = new Intent(context, Widget.class);
        intentEnabled.setAction(WIDGET_CLICKED);
        PendingIntent pendingIntentEnabled = PendingIntent.getBroadcast(context, 0, intentEnabled, 0);

        // Get the layout for the App Widget and attach an on-click listener
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);
        views.setOnClickPendingIntent(R.id.ivIcon, pendingIntentUi);
        views.setOnClickPendingIntent(R.id.tvStatus, pendingIntentEnabled);

        // update the enabled state on the widget
        boolean enabled = Settings.isNotify(context);
        boolean mute    = Settings.isMute(context);
        if(enabled){
            if(mute){
                views.setTextViewText(R.id.tvStatus, context.getText(R.string.widget_mute));
                views.setTextColor(R.id.tvStatus, context.getResources().getColor(R.color.widget_orange));
            }else{
                views.setTextViewText(R.id.tvStatus, context.getText(R.string.widget_enabled));
                views.setTextColor(R.id.tvStatus, context.getResources().getColor(R.color.widget_green));
            }
        }else{
            views.setTextViewText(R.id.tvStatus, context.getText(R.string.widget_disabled));
            views.setTextColor(R.id.tvStatus, context.getResources().getColor(R.color.widget_red));
        }

        // Tell the AppWidgetManager to perform an update on the current app widget
        appWidgetManager.updateAppWidget(widgetId, views);
    }

}
