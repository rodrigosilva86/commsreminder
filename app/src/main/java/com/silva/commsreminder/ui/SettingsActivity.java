package com.silva.commsreminder.ui;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.silva.commsreminder.R;

public class SettingsActivity extends PreferenceActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //TODO: add version dependant logic
        addPreferencesFromResource(R.xml.preferece_headers);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // this is to make sure that any changes made through the settings screen are reflected in the widget

        Intent intent = new Intent(this, Widget.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");

        // Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
        // since it seems the onUpdate() is only fired on that:
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
        int[] widgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(this, Widget.class));

        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIds);
        sendBroadcast(intent);
    }
}
