package com.silva.commsreminder.receiver;

import com.silva.commsreminder.service.ReminderService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

public class Receiver extends BroadcastReceiver {

	// private fields
	private static final String TAG = "Receiver";
	private static final int CPU_ON_PERIOD = 2000; // Time for wake lock duration

    private static PowerManager.WakeLock wakeLockCpu = null;

	// public methods
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		if(context == null){
			Log.e(TAG, "onReceive() called with null context");
			return;
		}
		if(intent == null){
			Log.e(TAG, "onReceive() called with null intent");
			return;
		}
		
		String action = intent.getAction();
		
		Log.d(TAG, "onReceive() called with action = " + action);

        // make sure that device does not falls asleep before the service is started/called
        // no action validation because it now also receives the Intents for ReminderQueue
        acquirePartialWakeLockWithTimeout(context);

        // call the service and add the extras, if available, so that ReminderQueue ID's
        // for notifications get passed along
        Intent serviceIntent = new Intent(context, ReminderService.class);
        Bundle extras = intent.getExtras();
        if(extras != null)
            serviceIntent.putExtras(extras);// pass the intent extras to the service
        context.startService(serviceIntent);
	}


    // private methods

    private void acquirePartialWakeLockWithTimeout(Context context){
        if(wakeLockCpu == null){
            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            wakeLockCpu = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        }
        wakeLockCpu.acquire(CPU_ON_PERIOD);// ensure CPU is running for service to process
    }

}
