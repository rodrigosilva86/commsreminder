package com.silva.commsreminder.main;

import android.app.Application;

import com.silva.commsreminder.R;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(
        formKey = "",
        formUri = "https://rodrigosilva.cloudant.com/acra-storage/_design/acra-storage/_update/report",
        reportType = org.acra.sender.HttpSender.Type.JSON,
        httpMethod = org.acra.sender.HttpSender.Method.PUT,
        formUriBasicAuthLogin="wateedidelciesecurindenc",
        formUriBasicAuthPassword="qeukYFJd3rKrmSRHG214ghlr",
        // Your usual ACRA configuration
//        mode = ReportingInteractionMode.TOAST,
//        resToastText = R.string.crash_dialog_text)
        mode = ReportingInteractionMode.DIALOG,
        resDialogIcon = R.drawable.telephone_black_128,
        resDialogTitle = R.string.crash_dialog_title,
        resDialogText = R.string.crash_dialog_text,
        resDialogCommentPrompt = R.string.crash_dialog_prompt,
        resDialogOkToast = R.string.crash_dialog_toast)
public class CommsReminder extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
    }
}
