package com.silva.commsreminder.interfaces;

public interface CallStatusChangeListener {

    /**
     * To notify of unseen calls
     *
     * @param pendingCallCount - number of pending calls
     */
    void onCallPending(int pendingCallCount);

    /**
     * To notify that all the calls have been seen/dismissed
     */
    void onCallDismissed();
}
