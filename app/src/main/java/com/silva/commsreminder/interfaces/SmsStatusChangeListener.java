package com.silva.commsreminder.interfaces;

public interface SmsStatusChangeListener {

    /**
     * To notify of unseen sms
     *
     * @param pendingSmsCount - number of pending sms
     */
    void onSmsPending(int pendingSmsCount);

    /**
     * To notify that all the sms have been seen/dismissed
     */
    void onSmsRead();
}
