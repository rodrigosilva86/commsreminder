package com.silva.commsreminder.interfaces;

public interface MmsStatusChangeListener {

    /**
     * To notify of unseen mms
     *
     * @param pendingMmsCount - number of pending mms
     */
    void onMmsPending(int pendingMmsCount);

    /**
     * To notify that all the mms have been seen/dismissed
     */
    void onMmsRead();
}
