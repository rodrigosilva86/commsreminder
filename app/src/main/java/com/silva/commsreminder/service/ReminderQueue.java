package com.silva.commsreminder.service;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.silva.commsreminder.R;
import com.silva.commsreminder.interfaces.CallStatusChangeListener;
import com.silva.commsreminder.interfaces.MmsStatusChangeListener;
import com.silva.commsreminder.interfaces.SmsStatusChangeListener;
import com.silva.commsreminder.observers.CallObserver;
import com.silva.commsreminder.observers.MmsObserver;
import com.silva.commsreminder.observers.SmsObserver;
import com.silva.commsreminder.receiver.Receiver;
import com.silva.commsreminder.settings.CallSettings;
import com.silva.commsreminder.settings.MmsSettings;
import com.silva.commsreminder.settings.Settings;
import com.silva.commsreminder.settings.SleepSettings;
import com.silva.commsreminder.settings.SmsSettings;

/**
 * This class manages all the Reminders.
 * All the methods are synchronized so that concurrent accesses (due to being called from content observers) will not cause inconsistencies.
 */
public class ReminderQueue implements CallStatusChangeListener, SmsStatusChangeListener, MmsStatusChangeListener {

    // private fields
    private static final String TAG = "ReminderQueue";

    private static final String ID_EXTRA    = "reminder_id";
    private static final int    ID_CALL     = 1;
    private static final int    ID_SMS      = 2;
    private static final int    ID_MMS      = 3;
    private static final int    ID_SLEEP    = 4;

    private static final long[] VIBRATE_PATTERN = {250, 250, 250, 250};// vibrate 2x 250ms

    private static final int SCREEN_ON_PERIOD = 5000;// turn screen on, dimmed, for 5 seconds

    private Context context;

    private ReminderService reminderService;

    private AlarmManager alarmManager;

    private PowerManager.WakeLock wakeLockScreen;

    private PendingIntent piCall;
    private PendingIntent piSms;
    private PendingIntent piMms;
    private PendingIntent piSleep;

    private boolean hasPendingCall;
    private boolean hasPendingSms;
    private boolean hasPendingMms;
    private boolean hasPendingNotification;

    private long pendingCallTime;
    private long pendingSmsTime;
    private long pendingMmsTime;


    // public methods

    public ReminderQueue(Context context, ReminderService reminderService){
        this.context = context;
        this.reminderService = reminderService;

        // get AlarmManager to wake up for notification
        alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        // get WakeLock to turn screen on
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLockScreen = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, TAG);

        // create Intents
        Intent iCall = new Intent(context, Receiver.class);
        Intent iSms = new Intent(context, Receiver.class);
        Intent iMms = new Intent(context, Receiver.class);
        Intent iSleep = new Intent(context, Receiver.class);

        // prepare intents
        iCall.putExtra(ID_EXTRA, ID_CALL);
        iSms.putExtra(ID_EXTRA, ID_SMS);
        iMms.putExtra(ID_EXTRA, ID_MMS);
        iSleep.putExtra(ID_EXTRA, ID_SLEEP);

        // create PendingIntent
        piCall = PendingIntent.getBroadcast(context, ID_CALL, iCall, 0);
        piSms = PendingIntent.getBroadcast(context, ID_SMS, iSms, 0);
        piMms = PendingIntent.getBroadcast(context, ID_MMS, iMms, 0);
        piSleep = PendingIntent.getBroadcast(context, ID_SLEEP, iSleep, 0);

        // set base values for flags
        hasPendingCall = false;
        hasPendingSms = false;
        hasPendingMms = false;
        hasPendingNotification = false;
    }

    public void checkIntent(Intent intent){
        int id = intent.getIntExtra(ID_EXTRA, 0);
        switch (id){
            case ID_CALL:
                Log.d(TAG, "Received intent with CALL ID");
                onCallReminderTime();
                break;
            case ID_SMS:
                Log.d(TAG, "Received intent with SMS ID");
                onSmsReminderTime();
                break;
            case ID_MMS:
                Log.d(TAG, "Received intent with MMS ID");
                onMmsReminderTime();
                break;
            case ID_SLEEP:
                Log.d(TAG, "Received intent with SLEEP ID");
                onSleepNotificationPressed();
                break;
            default:
                Log.d(TAG, "Received intent with invalid ID");
        }
    }

    public synchronized void stop(){
        onCallDismissed();
        onSmsRead();
        onMmsRead();
    }


    // call listener methods

    @Override
    public synchronized void onCallPending(int pendingCallsCount) {
        // check if reminder set for calls
        if(!hasPendingCall){
            hasPendingCall = true;

            // get settings for call reminder
            boolean notify                  = Settings.isNotify(context);
            boolean notifyOnPendingCalls    = CallSettings.isNotifyOnPendingCall(context);
            int notifyPeriod                = CallSettings.getNotifyPeriodOnPendingCall(context);

            //  prepare the reminder
            pendingCallTime = System.currentTimeMillis();
            if(notify && notifyOnPendingCalls){
                prepareReminder(notifyPeriod, piCall);
            }
        }
    }

    @Override
    public synchronized void onCallDismissed() {
        // clear reminders for call, if any
        alarmManager.cancel(piCall);
        hasPendingCall = false;
        // dismiss notification if there aren't reminders of any kind
        dismissSleepNotificationIfNoPendingReminders();
    }

    private void confirmCallStatus(){
        CallObserver callObserver = reminderService.getCallObserver();
        if(callObserver != null)
            hasPendingCall = callObserver.hasMissedCalls();
        // dismiss notification if there aren't reminders of any kind
        dismissSleepNotificationIfNoPendingReminders();
    }

    private void onCallReminderTime(){
        confirmCallStatus();

        // confirm that there are still pending call notifications
        if(hasPendingCall){
            // get the settings
            boolean notify                  = Settings.isNotify(context);
            boolean mute                    = Settings.isMute(context);
            boolean notifyOnPendingCalls    = CallSettings.isNotifyOnPendingCall(context);
            boolean vibrateOnPendingCalls   = CallSettings.isVibrateOnPendingCall(context);
            boolean ringOnPendingCalls      = CallSettings.isRingOnPendingCall(context);
            boolean ringOnCallStream        = CallSettings.isRingOnCallStream(context);
            boolean screenOnPendingCalls    = CallSettings.isScreenOnPendingCall(context);
            int pendingCallPeriod           = CallSettings.getNotifyPeriodOnPendingCall(context);
            int pendingCallTimeout          = CallSettings.getTimeoutOnPendingCall(context);
            boolean sleeping                    = SleepSettings.isSleeping(context);

            long now = System.currentTimeMillis();
            long timeout = pendingCallTimeout * 60 * 1000;// convert from minutes to milliseconds

            if(now > pendingCallTime + timeout){// check if expired
                onCallDismissed();
                return;
            }else{// prepare next
                prepareReminder(pendingCallPeriod, piCall);
            }

            // confirm that the user settings have not changed
            if(notify && notifyOnPendingCalls){

                if(sleeping){

                    if(screenOnPendingCalls){
                        screenOn(context);
                    }
                    showSleepNotification(context);

                }else{

                    if(vibrateOnPendingCalls){
                        vibrate(context);
                    }
                    if(ringOnPendingCalls && !mute){
                        ring(context, ringOnCallStream);
                    }
                    if(screenOnPendingCalls){
                        screenOn(context);
                    }
                }
            }
        }else{
            // no calls pending, then cancel the alarm
            onCallDismissed();
        }
    }


    // sms listener methods

    @Override
    public void onSmsPending(int pendingSmsCount) {
        // check if reminder set for messages
        if(!hasPendingSms){
            hasPendingSms = true;

            // get settings for message reminder
            boolean notify                  = Settings.isNotify(context);
            boolean notifyOnPendingMessage  = SmsSettings.isNotifyOnPendingMessage(context);
            boolean screenOnPendingMessages = SmsSettings.isScreenOnPendingMessage(context);
            boolean vibrateOnNewMessages    = SmsSettings.isVibrateOnNewMessage(context);
            int notifyPeriod                = SmsSettings.getNotifyPeriodOnPendingMessage(context);

            // turn screen on when SMS received
            if(screenOnPendingMessages){
                screenOn(context);
            }

            // vibrate when SMS received
            if(vibrateOnNewMessages){
                vibrate(context);
            }

            //  prepare the reminder
            pendingSmsTime = System.currentTimeMillis();
            if(notify && notifyOnPendingMessage){
                prepareReminder(notifyPeriod, piSms);
            }
        }
    }

    @Override
    public void onSmsRead() {
        // clear reminders for call, if any
        alarmManager.cancel(piSms);
        hasPendingSms = false;
        // dismiss notification if there aren't reminders of any kind
        dismissSleepNotificationIfNoPendingReminders();
    }

    private void confirmSmsStatus(){
        SmsObserver smsObserver = reminderService.getSmsObserver();
        if(smsObserver != null)
            hasPendingSms = smsObserver.hasPendingMessages();
        // dismiss notification if there aren't reminders of any kind
        dismissSleepNotificationIfNoPendingReminders();
    }

    private void onSmsReminderTime(){
        confirmSmsStatus();

        // confirm that there are still pending message notifications
        if(hasPendingSms){
            // get the settings
            boolean notify                      = Settings.isNotify(context);
            boolean mute                        = Settings.isMute(context);
            boolean notifyOnPendingMessages     = SmsSettings.isNotifyOnPendingMessage(context);
            boolean vibrateOnPendingMessages    = SmsSettings.isVibrateOnPendingMessage(context);
            boolean ringOnPendingMessages       = SmsSettings.isRingOnPendingMessage(context);
            boolean ringOnCallStream            = SmsSettings.isRingOnCallStream(context);
            boolean screenOnPendingMessages     = SmsSettings.isScreenOnPendingMessage(context);
            int pendingSmsPeriod                = SmsSettings.getNotifyPeriodOnPendingMessage(context);
            int pendingSmsTimeout               = SmsSettings.getTimeoutOnPendingMessage(context);
            boolean sleeping                    = SleepSettings.isSleeping(context);

            long now = System.currentTimeMillis();
            long timeout = pendingSmsTimeout * 60 * 1000;// convert from minutes to milliseconds

            if(now > pendingSmsTime + timeout){// check if expired
                onSmsRead();
                return;
            }else{// prepare next
                prepareReminder(pendingSmsPeriod, piSms);
            }

            // confirm that the user settings have not changed
            if(notify && notifyOnPendingMessages){

                if(sleeping){

                    if(screenOnPendingMessages){
                        screenOn(context);
                    }
                    showSleepNotification(context);

                }else{

                    if(vibrateOnPendingMessages){
                        vibrate(context);
                    }
                    if(ringOnPendingMessages && !mute){
                        ring(context, ringOnCallStream);
                    }
                    if(screenOnPendingMessages){
                        screenOn(context);
                    }

                }
            }
        }else{
            // no messages pending, then cancel the alarm
            onSmsRead();
        }
    }


    // mms listener methods

    @Override
    public void onMmsPending(int pendingMmsCount) {
        // check if reminder set for messages
        if(!hasPendingMms){
            hasPendingMms = true;

            // get settings for message reminder
            boolean notify                  = Settings.isNotify(context);
            boolean notifyOnPendingMessage  = MmsSettings.isNotifyOnPendingMessage(context);
            boolean screenOnPendingMessages = SmsSettings.isScreenOnPendingMessage(context);
            int notifyPeriod                = MmsSettings.getNotifyPeriodOnPendingMessage(context);

            // turn screen on when SMS received
            if(screenOnPendingMessages){
                screenOn(context);
            }

            //  prepare the reminder
            pendingMmsTime = System.currentTimeMillis();
            if(notify && notifyOnPendingMessage){
                prepareReminder(notifyPeriod, piMms);
            }
        }
    }

    @Override
    public void onMmsRead() {
        // clear reminders for call, if any
        alarmManager.cancel(piMms);
        hasPendingMms = false;
        // dismiss notification if there aren't reminders of any kind
        dismissSleepNotificationIfNoPendingReminders();
    }

    private void confirmMmsStatus(){
        MmsObserver mmsObserver = reminderService.getMmsObserver();
        if(mmsObserver != null)
            hasPendingMms = mmsObserver.hasPendingMessages();
        // dismiss notification if there aren't reminders of any kind
        dismissSleepNotificationIfNoPendingReminders();
    }

    private void onMmsReminderTime(){
        confirmMmsStatus();

        // confirm that there are still pending message notifications
        if(hasPendingSms){
            // get the settings
            boolean notify                      = Settings.isNotify(context);
            boolean mute                        = Settings.isMute(context);
            boolean notifyOnPendingMessages     = MmsSettings.isNotifyOnPendingMessage(context);
            boolean vibrateOnPendingMessages    = MmsSettings.isVibrateOnPendingMessage(context);
            boolean ringOnPendingMessages       = MmsSettings.isRingOnPendingMessage(context);
            boolean ringOnCallStream            = MmsSettings.isRingOnCallStream(context);
            boolean screenOnPendingMessages     = MmsSettings.isScreenOnPendingMessage(context);
            int pendingMmsPeriod                = MmsSettings.getNotifyPeriodOnPendingMessage(context);
            int pendingMmsTimeout               = MmsSettings.getTimeoutOnPendingMessage(context);
            boolean sleeping                    = SleepSettings.isSleeping(context);

            long now = System.currentTimeMillis();
            long timeout = pendingMmsTimeout * 60 * 1000;// convert from minutes to milliseconds

            if(now > pendingMmsTime + timeout){// check if expired
                onSmsRead();
                return;
            }else{// prepare next
                prepareReminder(pendingMmsPeriod, piMms);
            }

            // confirm that the user settings have not changed
            if(notify && notifyOnPendingMessages){

                if(sleeping){

                    if(screenOnPendingMessages){
                        screenOn(context);
                    }
                    showSleepNotification(context);

                }else{

                    if(vibrateOnPendingMessages){
                        vibrate(context);
                    }
                    if(ringOnPendingMessages && !mute){
                        ring(context, ringOnCallStream);
                    }
                    if(screenOnPendingMessages){
                        screenOn(context);
                    }

                }
            }
        }else{
            // no messages pending, then cancel the alarm
            onMmsRead();
        }
    }


    // utility methods

    private void prepareReminder(int notifyPeriod, PendingIntent pi){
        long now = SystemClock.elapsedRealtime();
        long period = notifyPeriod * 60 * 1000; // convert from minutes to milliseconds
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, now + period, pi);
        // setRepeating would not detect period changes
    }

    private void vibrate(Context context){
        if(isInCall(context)){
            Log.d(TAG, "vibrate() ignored due to being in call");
        }else{
            Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(VIBRATE_PATTERN, -1);
        }
    }

    private void ring(Context context, boolean callStream){
        if(isInCall(context)){
            Log.d(TAG, "ring() ignored due to being in call");
        }else{
        Uri ringUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if(ringUri != null){
                Ringtone ring = RingtoneManager.getRingtone(context, ringUri);
                if(ring != null){
                    if(callStream)
                        ring.setStreamType(AudioManager.STREAM_RING);
                    ring.play();
                }else{
                    Log.d(TAG, "Default ring tone is not available");
                }
            }else{
                Log.d(TAG, "Default ring tone Uri is not available");
            }
        }
    }

    private void screenOn(Context context){
        wakeLockScreen.acquire(SCREEN_ON_PERIOD);
    }

    private boolean isInCall(Context context){
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        int callState = telephonyManager.getCallState();
        return callState != TelephonyManager.CALL_STATE_IDLE;
    }

    private void showSleepNotification(Context context){

        if(hasPendingNotification){

            Log.d(TAG, "Notification already visible");

        }else{

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
            notificationBuilder.setSmallIcon(R.drawable.telephone_black_72);
            notificationBuilder.setContentTitle(context.getString(R.string.notification_title));
            notificationBuilder.setContentText(context.getString(R.string.notification_message));

            notificationBuilder.setContentIntent(piSleep);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(ID_SLEEP, notificationBuilder.build());

            hasPendingNotification = true;
        }

    }

    private void onSleepNotificationPressed(){

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        hasPendingNotification = false;

    }


    private void dismissSleepNotificationIfNoPendingReminders(){
        if(!hasPendingCall && !hasPendingSms && !hasPendingMms)
            onSleepNotificationPressed();
    }
}
