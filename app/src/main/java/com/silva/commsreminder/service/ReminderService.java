package com.silva.commsreminder.service;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.silva.commsreminder.observers.CallObserver;
import com.silva.commsreminder.observers.MmsObserver;
import com.silva.commsreminder.observers.SmsObserver;

public class ReminderService extends Service {

    // private fields
    private static final String TAG = "ReminderService";

    private ReminderQueue reminderQueue;
    private CallObserver callObserver = null;
    private SmsObserver smsObserver = null;
    private MmsObserver mmsObserver = null;

    // public methods

	@Override
	public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind()");
        // nothing needed here
        return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
        Log.d(TAG, "onCreate()");

        reminderQueue = new ReminderQueue(getApplicationContext(), this);

        try{

            Context context = getApplicationContext();
            if(context == null){
                Log.e(TAG, "onCreate() could not get Context");
                return;
            }
            ContentResolver contentResolver = context.getContentResolver();
            if(contentResolver == null){
                Log.e(TAG, "onCreate() could not get ContentResolver");
                return;
            }

            registerCallObserver(contentResolver);
            registerSmsObserver(contentResolver);
            registerMmsObserver(contentResolver);

        }catch(NullPointerException e){
            Log.d(TAG, "onCreate() failed", e);
        }
	}

    @Override
	public void onDestroy() {
		super.onDestroy();
        Log.d(TAG, "onDestroy()");

        reminderQueue.stop();

        try{

            Context context = getApplicationContext();
            if(context == null){
                Log.e(TAG, "onCreate() could not get Context");
                return;
            }
            ContentResolver cr = context.getContentResolver();
            if(cr == null){
                Log.e(TAG, "onCreate() could not get ContentResolver");
                return;
            }

            unregisterCallObserver(cr);
            unregisterSmsObserver(cr);
            unregisterMmsObserver(cr);

        }catch(NullPointerException e){
            Log.e(TAG, "onDestroy() failed", e);
        }
	}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // the only source for this is the reminder queue, so the intent only gets passed there
        if(intent != null)
            reminderQueue.checkIntent(intent);
        return START_STICKY;
    }



    public CallObserver getCallObserver() {
        return callObserver;
    }

    public SmsObserver getSmsObserver() {
        return smsObserver;
    }

    public MmsObserver getMmsObserver() {
        return mmsObserver;
    }

    // private methods

    private void registerCallObserver(ContentResolver contentResolver){
        callObserver = new CallObserver(new Handler(), contentResolver, reminderQueue);
        callObserver.checkForMissedCalls();
        contentResolver.registerContentObserver(CallObserver.CONTENT_URI, true, callObserver);
    }

    private void unregisterCallObserver(ContentResolver contentResolver){
        if(callObserver != null){
            contentResolver.unregisterContentObserver(callObserver);
            callObserver = null;
        }
    }

    private void registerSmsObserver(ContentResolver contentResolver){
        smsObserver = new SmsObserver(new Handler(),contentResolver, reminderQueue );
        smsObserver.checkForPendingMessages();
        contentResolver.registerContentObserver(SmsObserver.CONTENT_URI, true, smsObserver);
    }

    private void unregisterSmsObserver(ContentResolver contentResolver){
        if(smsObserver != null){
            contentResolver.unregisterContentObserver(smsObserver);
            smsObserver = null;
        }
    }

    private void registerMmsObserver(ContentResolver contentResolver){
        mmsObserver = new MmsObserver(new Handler(),contentResolver, reminderQueue );
        mmsObserver.checkForPendingMessages();
        contentResolver.registerContentObserver(MmsObserver.CONTENT_URI, true, mmsObserver);
    }

    private void unregisterMmsObserver(ContentResolver contentResolver){
        if(mmsObserver != null){
            contentResolver.unregisterContentObserver(mmsObserver);
            mmsObserver = null;
        }
    }
}
